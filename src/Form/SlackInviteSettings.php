<?php

/**
 * @file
 * Contains \Drupal\slack_invite\Form\SlackInviteSettings.
 */

namespace Drupal\slack_invite\Form;

use Drupal\Core\Form\ConfigFormBase;
use Drupal\Core\Form\FormStateInterface;

/**
 * Class SlackInviteSettings.
 *
 * @package Drupal\slack_invite\Form
 */
class SlackInviteSettings extends ConfigFormBase
{

  /**
   * {@inheritdoc}
   */
  protected function getEditableConfigNames()
  {
    return [
      'slack_invite.slackinvitesettings'
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function getFormId()
  {
    return 'slack_invite_settings';
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state)
  {
    $config = $this->config('slack_invite.slackinvitesettings');
    $form['team_name'] = array(
      '#type' => 'textfield',
      '#title' => $this->t('Team name'),
      '#description' => $this->t('The name of your slack team'),
      '#default_value' => $config->get('team_name'),
    );
    $form['slack_url'] = array(
      '#type' => 'textfield',
      '#title' => $this->t('Slack url'),
      '#description' => $this->t('The url of your slack team (e.g. teamname.slack.com)'),
      '#default_value' => $config->get('slack_url'),
    );
    $form['slack_api_token'] = array(
      '#type' => 'textfield',
      '#title' => $this->t('Slack API token'),
      '#description' => $this->t('DO NOT generate an API key as a team owner. Create or use an admin account to generate an API key at https://api.slack.com/web#auth'),
      '#default_value' => $config->get('slack_api_token'),
    );

    return parent::buildForm($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  public function validateForm(array &$form, FormStateInterface $form_state)
  {
    parent::validateForm($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state)
  {
    parent::submitForm($form, $form_state);

    $this->config('slack_invite.slackinvitesettings')
      ->set('team_name', $form_state->getValue('team_name'))
      ->set('slack_url', $form_state->getValue('slack_url'))
      ->set('slack_api_token', $form_state->getValue('slack_api_token'))
      ->save();
  }

}
