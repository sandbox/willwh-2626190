<?php

/**
 * @file
 * Contains \Drupal\slack_invite\Form\SlackInviteSettings.
 */

namespace Drupal\slack_invite\Form;

use Drupal\Component\Utility\UrlHelper;
use Drupal\Core\Form\FormBase;
use Drupal\Core\Form\FormStateInterface;
use Symfony\Component\Config\Definition\Exception\Exception;

/**
 * Class SlackInviteSettings.
 *
 * @package Drupal\slack_invite\Form
 */
class SlackInviteForm extends FormBase
{

  /**
   * {@inheritdoc}
   */
  public function getFormId()
  {
    return 'slack_invite.slack_invite_form';
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state)
  {
    $config = $this->config('slack_invite.slackinvitesettings');
    $team = $config->get('team_name');
    $slackUrl = $config->get('slack_url');
    $slackToken = $config->get('slack_api_token');
    if ((!UrlHelper::isValid($slackUrl, TRUE)) && !$team && !$slackToken) {
      drupal_set_message(t('To configure this Module, go to <a href="@slack_invite">the settings form</a> in the config menu.', array('@slack_invite' => 'admin/config/slack_invite/slackinvitesettings')));
    }
    $user_email = \Drupal::currentUser()->getEmail();
    $form['email'] = array(
      '#type' => 'textfield',
      '#description' => 'Enter your email to be invited to the ' . $team . ' slack team',
      '#default_value' => $user_email
    );
    $form['invite'] = array(
      '#type' => 'submit',
      '#value' => 'Invite'
    );
    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function validateForm(array &$form, FormStateInterface $form_state)
  {
    $email = $form_state->getValue('email');
    if (!\Drupal::service('email.validator')->isValid($email)) {
      $form_state->setErrorByName('email', 'Your email is invalid.');
    }
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state)
  {
    $config = $this->config('slack_invite.slackinvitesettings');
    $slack = 'https://' . $config->get('slack_url') . '/api/users.admin.invite';
    $team = $config->get('team_name');
    if (!empty($config->get('slack_api_token'))) {
      $client = \Drupal::httpClient();
      try {
        $request = $client->request('POST', $slack, [
          'form_params' => [
            'email' => $form_state->getValue('email'),
            'token' => $config->get('slack_api_token'),
            'set_active' => true
          ]
        ]);
        $response = json_decode($request->getBody());
        $code = $request->getStatusCode();
        if ($code == "200") {
          if ($response->ok == true) {
            drupal_set_message(t('Success! Check ' . $form_state->getValue('email') . ' for an invite from Slack.'));
          } else {
            $error = $response->error;
            if ($error == 'already_invited' || $error == 'already_in_team') {
              drupal_set_message(t('You have already been invited. Check ' . $form_state->getValue('email') . ' for an invite from the ' . $team . ' slack team.'));
            }
          }
        } else {
          // Things are NOT okay.
          $error = $request->getBody()->getContents();
          drupal_set_message($error['error']);
        }
      } catch (Exception $e) {
        watchdog_exception('slack_invite', $e->getMessage());
      }
    }
  }
}